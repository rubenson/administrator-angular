import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  getUser(){
    return [
        {"id":1,"name":"Asep saipuloh", "age":20 },
        {"id":2,"name":"Zaenal", "age":22 },
        {"id":3,"name":"Kurdi", "age":19 },
        {"id":4,"name":"Heni kurniawati", "age":23 },
        {"id":5,"name":"Wanti", "age":28 },
        {"id":6,"name":"Zaki", "age":30 }
    ]
  }
}
