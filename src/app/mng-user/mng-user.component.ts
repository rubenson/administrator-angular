import { Component, TemplateRef, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-mng-user',
  templateUrl: './mng-user.component.html',
  styleUrls: ['./mng-user.component.css'],
})
export class MngUserComponent implements OnInit {

  public users = [];
  public title = "TABEL USER";
  modalRef: BsModalRef;
  config = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false,
    class: "my-modal"
  };

  constructor(private _userServices: UserService, private modalService: BsModalService) { }

  ngOnInit(): void {
      // get user from user service
      this.users = this._userServices.getUser();
  }
  
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,
        Object.assign(
          {}, { class: 'my-modal gray modal-lg' }
        )
      );
  }

}
