import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SideNavComponent } from './side-nav/side-nav.component';
import { HomeComponent } from './home/home.component';
import { MngUserComponent } from './mng-user/mng-user.component';
import { MngContentComponent } from './mng-content/mng-content.component';


const routes: Routes = [
  { path: '', component: SideNavComponent, children:[
      {path:'home', component:HomeComponent},
      {path:'management-user', component:MngUserComponent},
      {path:'management-content', component:MngContentComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
