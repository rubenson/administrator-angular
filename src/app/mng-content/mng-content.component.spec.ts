import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MngContentComponent } from './mng-content.component';

describe('MngContentComponent', () => {
  let component: MngContentComponent;
  let fixture: ComponentFixture<MngContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MngContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MngContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
